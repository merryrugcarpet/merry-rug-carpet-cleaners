Since 1946 Merry Rug Cleaners has been providing superior service in Old World Craftmanship to the South Florida area. Merry Rug Cleaners will pick up your area rugs and clean on our premises, or if you prefer, cleaning can be done in your home or office.

Address: 5701 Georgia Ave, West Palm Beach, FL 33405, USA
Phone: 561-588-8588
